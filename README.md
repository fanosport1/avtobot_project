How to run project
1. create venv python3 -m venv venv
2. install requirements.txt
3. run docker-compose 
4. python3 manage.py makemigrations
5. python3 manage.py migrate
6. python3 manage.py runserver 
7. start avtobot -- open file, in config write value then go to terminal and run this file