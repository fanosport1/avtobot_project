import requests
import names
import random
import string

confi = {
    'number_of_users': 4,
    'max_posts_per_user': 3,
    'max_likes_per_user': 3
}


class AutomatedBOT(object):
    def __init__(self):
        self.access_toke = None
        self.user_pk = None

    @staticmethod
    def random_char(len_email):
        return ''.join(random.choice(string.ascii_letters) for x in range(len_email))

    def _create_user(self):
        reqs_create_user = requests.post(
            url='http://127.0.0.1:8000/auth/sign-up/',
            json={
                "email": f"{self.random_char(20).lower()}@test.com",
                "password": "Fano1a2s3d",
                "username": self.random_char(20),
                "first_name": self.random_char(20)
            }
        )
        self.user_pk = reqs_create_user.json()["id"]
        print('reqs_create_user')
        reqs_login = requests.post('http://127.0.0.1:8000/auth/login/', json={
            "username": reqs_create_user.json()["username"],
            "password": "Fano1a2s3d"
        })
        self.access_toke = reqs_login.json()["access"]
        print('reqs_login')

    def _create_post(self, posts):
        print('_create_post')
        for post in range(posts):
            requests.post(
                url='http://127.0.0.1:8000/post/post/',
                json={
                    "name": self.random_char(10),
                    "owner": self.user_pk
                },
                headers={'Authorization': f'JWT {self.access_toke}'}
              )

    def get_post(self):
        reqs_get_post = requests.get(
            url='http://127.0.0.1:8000/post/post/',
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'JWT {}'.format(self.access_toke)
            }
        )
        return [x['pk'] for x in reqs_get_post.json()['results']]

    def _add_like(self, likes):
        all_posts = self.get_post()
        for like in range(likes):
            if not all_posts:
                return "Not enough post"
            post_pk = all_posts.pop(0)
            requests.post(
                url=f'http://127.0.0.1:8000/post/post/{post_pk}/like/',
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': 'JWT {}'.format(self.access_toke)
                }
            )
            print('_add_like')

    def statr(self, posts, like):
        self._create_user()
        self._create_post(posts)
        self._add_like(like)


def start_config(config):
    for user in range(config['number_of_users']):
        AutomatedBOT().statr(posts=config['max_posts_per_user'], like=config['max_likes_per_user'])


start_config(confi)
