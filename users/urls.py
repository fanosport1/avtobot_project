from rest_framework.routers import SimpleRouter
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView

from users.views import *

router = SimpleRouter()

router.register('users', UserViewSet, basename='users')

urlpatterns = [
    path('', include(router.urls)),
    path('sign-up/', SignUpView.as_view(), name='sign-up'),
    path('login/', TokenObtainPairView.as_view(), name='login'),
]
