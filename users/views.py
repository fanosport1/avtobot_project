from rest_framework import generics, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated

from users.models import UserProfile
from users.serializers import SignUpSerializer, UserSerializer
from rest_framework.authentication import TokenAuthentication, SessionAuthentication


class SignUpView(generics.CreateAPIView):
    """
    post:
    Create new user
    Register a user with obtained params
    """
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = [TokenAuthentication, SessionAuthentication]
