from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password

from users.models import UserProfile


class SignUpSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, validators=[validate_password])

    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'password', 'username', 'first_name')

    @staticmethod
    def validate_email(value):
        return value.lower()

    def create(self, validated_data):
        user = UserProfile.objects.create_user(
            username=validated_data.get('first_name'),
            first_name=validated_data.get('first_name'),
            email=validated_data.get('email'),
            password=validated_data.get('password'),
        )

        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('id', 'email', 'first_name')
