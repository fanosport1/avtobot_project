from django.db import models
from django.contrib.auth.models import User, UserManager
from django.utils import timezone


class UserProfile(User):


    objects = UserManager()

    class Meta:
        db_table = 'user_profile'
