from rest_framework import serializers

from posts.models import Post, Like


class ListPostsSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField()
    is_liked_by_user = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['pk', 'name', 'date_create', 'likes', 'is_liked_by_user']

    @staticmethod
    def get_likes(obj):
        return obj.liked_post.count()

    def get_is_liked_by_user(self, obj):
        return self.context['request'].user.pk in [like.user_like.pk for like in obj.liked_post.all()]


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['pk', 'name', 'owner']

    def to_internal_value(self, data):
        data['owner'] = self.context.get('request').user.pk
        return super().to_internal_value(data)


class PostDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['pk', 'name', 'owner', 'date_create']


class ListAnaliticsPostsSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['pk', 'name', 'date_create', 'likes']

    def get_likes(self, obj):
        request_query_params = self.context['request'].query_params
        if request_query_params:
            return obj.liked_post.filter(
                date_create__range=[request_query_params['date_from'], request_query_params['date_to']]
            ).count()
