from django.db import models

# Create your models here.
from django.utils import timezone

from users.models import UserProfile


class Post(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='own_3dmodels')
    date_create = models.DateTimeField(blank=True, null=True, default=timezone.now)
    name = models.CharField(max_length=200)

    class Meta:
        db_table = 'post'

    def __str__(self):
        return self.name


class Like(models.Model):
    user_like = models.ForeignKey(UserProfile, related_name='user_liked', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='liked_post', on_delete=models.CASCADE)
    date_create = models.DateTimeField(blank=True, null=True, default=timezone.now)

    class Meta:
        db_table = 'like'
        unique_together = ('post', 'user_like')
