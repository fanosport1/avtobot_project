from django.urls import path, include
from rest_framework.routers import SimpleRouter

from posts.views import *

router = SimpleRouter()

router.register('post', PostViewSet, basename='post')

urlpatterns = [
    path('', include(router.urls)),
    path('analitics/', PostAnaliticsView.as_view(), name='analitics'),
]
