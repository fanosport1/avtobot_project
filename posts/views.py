from rest_framework import viewsets, status, generics
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from posts.mixins import GetSerializerClassMixin
from posts.models import Post, Like
from posts.serializers import ListPostsSerializer, PostSerializer, PostDetailSerializer, ListAnaliticsPostsSerializer
from users.models import UserProfile


class PostViewSet(GetSerializerClassMixin, viewsets.ModelViewSet):
    queryset = Post.objects.prefetch_related('liked_post')
    serializer_class = PostDetailSerializer
    serializer_action_classes = {
        'list': ListPostsSerializer,
        'create': PostSerializer,
    }
    permission_classes = [IsAuthenticatedOrReadOnly]

    @action(methods=['POST'], detail=True, permission_classes=[IsAuthenticated])
    def like(self, request, *args, **kwargs):
        user = UserProfile.objects.get(id=self.request.user.id)
        post = Post.objects.get(id=self.kwargs['pk'])
        likes, create = Like.objects.get_or_create(post=post, user_like=user)
        if create:
            return Response(status=status.HTTP_201_CREATED)
        likes.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PostAnaliticsView(generics.ListAPIView):
    queryset = Post.objects.prefetch_related('liked_post')
    serializer_class = ListAnaliticsPostsSerializer
